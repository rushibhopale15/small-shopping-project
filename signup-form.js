document.getElementById("signup").addEventListener("submit", function (event) {
    event.preventDefault(); 

    let firstName = document.getElementById('first-name').value;
    let lastName = document.getElementById('last-name').value;
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    let repeatPassword = document.getElementById('repeat-password').value;
    let tos = document.getElementById('tos').checked;

    let correctMessage = [];
    let pattern = /[a-zA-Z]/;
    if (firstName.length < 3 || pattern.test(firstName) == false) {
        document.querySelector('#error1').style.display = 'block';
    }
    if(firstName.length>3 && pattern.test(firstName) == true){
        document.querySelector('#error1').style.display = 'none';
        correctMessage.push('#error1');
    }
    if(lastName.length < 3 || pattern.test(lastName) == false){
        document.querySelector('#error2').style.display = 'block';
    }
    if(lastName.length>3 && pattern.test(lastName)){
        document.querySelector('#error2').style.display = 'none';
        correctMessage.push('#error2');
    }
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (emailPattern.test(email) == false) {
        document.querySelector('#error3').style.display = 'block';
    }
    if (emailPattern.test(email)) {
        document.querySelector('#error3').style.display = 'none';
        correctMessage.push('#error3');
    }

    if (password.length <5 || !repeatPassword.length <5) {
        document.querySelector('#error4').style.display = 'block';
    }
    if (password.length > 4 || repeatPassword.length > 4) {
        document.querySelector('#error4').style.display = 'none';
        correctMessage.push('#error4');
    }
    if (password != repeatPassword) {
        document.querySelector('#error5').style.display = 'block';
    }
    if (password == repeatPassword) {
        document.querySelector('#error5').style.display = 'none';
        correctMessage.push('#error5');
    }
    if (!tos) {
        document.querySelector('.tos').style.color = 'red';
    }
    if (tos) {
        document.querySelector('.tos').style.color = 'black';
        correctMessage.push(`read and accept terms and conditions`);
    }
    // console.log(correctMessage.length);
    if(correctMessage.length == 6){
        window.location.href = 'index.html';
    }
});

function clearData() {
    document.querySelector('.error-div').innerHTML = '';
    document.getElementById('first-name').value = '';
    document.getElementById('last-name').value = '';
    document.getElementById('email').value = '';
    document.getElementById('password').value = '';
    document.getElementById('repeat-password').value = '';
    document.getElementById('tos').checked = '';
}